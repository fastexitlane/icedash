export const DELIMITER = "";
export const TIMEOUT_GAME = 80; // 10 fps
export const TIMEOUT_RENDER = 50; // 20 fps
export const TIMEOUT_CELLCONSUME = 240;
export const TIMEOUT_GAMENOTIFICATIONS = 2000;
export const LEVEL_TIME = 120000; // time in milliseconds in which each level has to be completed
export const TYPES = {
    BOULDER:    "BOULDER",
    DIRT:       "DIRT",
    EMPTY:      "EMPTY",
    GEM:        "GEM",
    PLAYER:     "PLAYER",
    WALL:       "WALL",
    EXIT:       "EXIT",
    BOMB:       "BOMB",
};
export const TYPESYMBOLS = {
    BOULDER:    "R",
    DIRT:       "#",
    EMPTY:      "E",
    GEM:        "D",
    PLAYER:     "X",
    WALL:       "W",
    EXIT:       "P",
    BOMB:       "F",
};
export const ASSETS = {
    BOULDER:    {src: "assets/images/rock.png",        img: new Image(),   sprite: false},
    DIRT:       {src: "assets/images/dirt.png",        img: new Image(),   sprite: false},
    EMPTY:      {src: "assets/images/bg.png",          img: new Image(),   sprite: false},
    GEM:        {src: "assets/images/coin.png",        img: new Image(),   sprite: true,   sprites: 14,    frame_increase: 0.7,    frame: 0},
    PLAYER:     {src: "assets/images/player_right.png",img: new Image(),   sprite: true,   sprites: 8,     frame_increase: 0.45,   frame: 0},
    PLAYER_LEFT:{src: "assets/images/player_left.png", img: new Image(),   sprite: true,   sprites: 8,     frame_increase: 0.45,   frame: 0},
    WALL:       {src: "assets/images/castle.png",      img: new Image(),   sprite: false},
    EXIT:       {src: "assets/images/flag.png",        img: new Image(),   sprite: true,   sprites: 2,     frame_increase: 0.5,   frame: 0},
    INVISIBLE:  {src: "assets/images/invisible.png",   img: new Image(),   sprite: false},
    BOMB:       {src: "assets/images/bomb.png",        img: new Image(),   sprite: false},
    EXPLOSION:  {src: "assets/images/explosion.png",   img: new Image(),   sprite: true,   sprites: 64,    frame_increase: 0.5,     frame: 0},
};
export const LEVEL = {
    1:          {src: "assets/level/level-01.dash", title: "Bombs and Boulders"},
    2:          {src: "assets/level/level-02.dash", title: "Double You See"},
    3:          {src: "assets/level/level-03.dash", title: "Grumpys Cave"},
};
export const STARTLEVEL = 1;
export const AUDIOMUTEDONLOAD = true;
