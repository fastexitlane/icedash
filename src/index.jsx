import preload from "./loading/Preloader";
import {assetsLoaded, lastLevel, lvlWin, mute, nextLevel, pause, playerDied, playerTrapped, restartLevel, resume, start, startFromLvlUpload, startLevel, timeOver} from "./game/AppControls";

// sounds
export let menuMusic, gameMusic, explosionSound, gemCollectedSound, gameWinSound, boulderHitSound ;

// divs and other elements
export let appContainer, lvlUploadWrapper, infoPaused;

// buttons
export let btnStart, btnPause, btnResume, btnNextLvl, btnLastLvl, lvlUpload, btnRestartLevel, btnMute;

window.addEventListener("load", () => {
    // initialize the DOM representation
    initDOM();

    // mute audio
    mute();

    // preload the game assets
    preload();

    // initialize start screen and level start / upload buttons when assets are loaded
    initializeLevelLoadingListeners();

    // initialize game controls and their functionality when a level is loaded
    initializeGameControlListeners();

    // initialize event listeners for sound effects
    initializeSoundEffectListeners();

    // intitialize event listeners for game status events
    initializeGameStatusListeners();
});

const initDOM = () => {
    menuMusic = document.getElementById("menuSound");
    gameMusic = document.getElementById("gameRunningSound");
    gameWinSound = document.getElementById("gameWinSound");
    explosionSound = document.getElementById("explosionSound");
    gemCollectedSound = document.getElementById("gemCollectedSound");
    boulderHitSound = document.getElementById("boulderHitSound");

    appContainer = document.getElementById("appContainer");
    lvlUploadWrapper = document.getElementById("lvlUploadWrapper");
    infoPaused = document.getElementById("infoPaused");

    btnStart = document.getElementById("btnStart");
    btnPause = document.getElementById("btnPause");
    btnResume = document.getElementById("btnResume");
    btnNextLvl = document.getElementById("btnNextLvl");
    btnLastLvl = document.getElementById("btnLastLvl");
    lvlUpload = document.getElementById("lvlUpload");
    btnRestartLevel = document.getElementById("btnRestartLevel");
    btnMute = document.getElementById("btnMute");
};

const initializeGameControlListeners = () => {
    // show the splash screen and the pause controls when paused
    btnPause.addEventListener("click", pause);

    // restart the game loop and display the game controls
    btnResume.addEventListener("click", resume);

    // add event listeners to the level switch buttons
    btnNextLvl.addEventListener("click", nextLevel);
    btnLastLvl.addEventListener("click", lastLevel);
    btnRestartLevel.addEventListener("click", restartLevel);

    // add event listener to mute button
    btnMute.addEventListener("click", mute);
};

const initializeLevelLoadingListeners = () => {
    // display the splash screen and the start button when the assets are loaded
    appContainer.addEventListener("assetsLoaded", assetsLoaded);

    // load the first level when start is clicked
    btnStart.addEventListener("click", start);

    // start from level upload
    lvlUpload.addEventListener("change", startFromLvlUpload);

    // start the game loop and show the game controls when the level is loaded
    appContainer.addEventListener("levelLoaded", startLevel);
};

const initializeSoundEffectListeners = () => {
    // bomb explosion
    appContainer.addEventListener("explosion", () => {
        explosionSound.play();
        explosionSound.currentTime = 0;
    });

    // player died
    appContainer.addEventListener("playerDied", () => {
        explosionSound.play();
        explosionSound.currentTime = 0;
    });

    // player collects gem
    appContainer.addEventListener("gemCollected", () => {
        gemCollectedSound.play();
        gemCollectedSound.currentTime = 0;
    });

    // player exited
    appContainer.addEventListener("lvlWin", () => {
        gameMusic.pause();
        gameMusic.currentTime = 0;
        gameWinSound.play();
    });

    // boulder hits ground
    appContainer.addEventListener("boulderHit", () => {
        boulderHitSound.volume = 0.3;
        boulderHitSound.play();
        boulderHitSound.currentTime = 0;
    });

    // gem hits ground
    appContainer.addEventListener("gemHit", () => {
        boulderHitSound.volume = 0.3;
        boulderHitSound.play();
        boulderHitSound.currentTime = 0;
    });

    // TODO: sounds for player move
};

const initializeGameStatusListeners = () => {
    appContainer.addEventListener("playerDied", playerDied);
    appContainer.addEventListener("lvlWin", lvlWin);
    appContainer.addEventListener("timeOver", timeOver);
    appContainer.addEventListener("playerTrapped", playerTrapped);
};
