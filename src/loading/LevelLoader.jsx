import LevelModel from "../model/LevelModel";
import {DELIMITER, LEVEL, TYPESYMBOLS} from "../config";
import PlayerModel from "../model/celltypes/PlayerModel";
import WallModel from "../model/celltypes/WallModel";
import BoulderModel from "../model/celltypes/BoulderModel";
import EmptyModel from "../model/celltypes/EmptyModel";
import DirtModel from "../model/celltypes/DirtModel";
import GemModel from "../model/celltypes/GemModel";
import ExitModel from "../model/celltypes/ExitModel";
import Point from "../model/field/Point";
import {renderLevelKeyTitle} from "../game/Renderer";
import BombModel from "../model/celltypes/BombModel";

export let currentLevel = null;
export let fileShadowCopy = null;

export const loadLevel = (levelKey) => {
    getFile(levelKey)
        .then((file) => {
            fileShadowCopy = file;
            return fileShadowCopy;
        })
        .then(getStringFromFile)
        .then(parseIntoArray)
        .then((result) => {
            currentLevel = new LevelModel(result, levelKey, LEVEL[levelKey].title);
        })
        .then(() => console.log(currentLevel))
        .then(() => renderLevelKeyTitle(currentLevel))
        .then(fireLoadEvent)
        .catch((err) => {
            alert(err);
            location.reload();
        }
    );
};

export const loadLevelFromFile = (file) => {
    fileShadowCopy = file;
    getStringFromFile(file)
        .then(manipulateLineEndings)
        .then(convertToUpperCase)
        //.then(isValid)
        .then(parseIntoArray)
        .then((result) => {
            currentLevel = new LevelModel(result);
        })
        .then(() => console.log(currentLevel))
        .then(() => renderLevelKeyTitle(currentLevel))
        .then(fireLoadEvent)
        .catch((err) => {
            alert(err);
            location.reload();
        }
    );
};

export const loadLevelFromShadowCopy = (externalFileMode, levelKey) => {
    getStringFromFile(fileShadowCopy)
        .then((gameDataString) => {
            if (externalFileMode) {
                return convertToUpperCase(manipulateLineEndings(gameDataString));
            }
            else {
                return gameDataString;
            }
        })
        .then(parseIntoArray)
        .then((result) => {
            if (externalFileMode) {
                currentLevel = new LevelModel(result);
            }
            else {
                currentLevel = new LevelModel(result, levelKey, LEVEL[levelKey].title);
            }
        })
        .then(() => console.log(currentLevel))
        .then(() => renderLevelKeyTitle(currentLevel))
        .then(fireLoadEvent)
        .catch((err) => {
            alert(err);
            location.reload();
        }
    );
};

const getFile = (levelKey) => {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open("GET", LEVEL[levelKey].src);
        xhr.responseType = "blob";
        xhr.addEventListener("load", () => {
            resolve(xhr.response);
        });
        xhr.addEventListener("error", reject);

        xhr.send();
    });
};

const getStringFromFile = (file) => {
    return new Promise((resolve, reject) => {
        let reader = new FileReader();
        reader.addEventListener("load", () => {
            resolve(reader.result);
        });
        reader.addEventListener("error",reject);
        reader.readAsText(file);
    });
};

const manipulateLineEndings = (gameDataString) => {
    return gameDataString.replace(/\r\n|\r/g, "\n");
};

const convertToUpperCase = (gameDataString) => {
    return gameDataString.toUpperCase();
};

const isValid = (gameDataString) => {
    if (gameDataString === "") {
        throw "Empty File!";
    }

    gameDataString.split("\n").forEach((row, rownr) => {
        for (let i=0; i<row.length; i++) {
            if (row[i] !== DELIMITER
                && !(Object.keys(TYPESYMBOLS).map((field) => TYPESYMBOLS[field]).includes(row[i]))) {
                throw "Invalid Character at line "+(rownr+1)+" column "+(i+1)+"!";
            }
        }
    });

    return gameDataString;
};

const parseIntoArray = (gameDataString) => {
    return (gameDataString.split("\n")
            .map((row, y) => {
                let tmp = row.split(DELIMITER);
                tmp.forEach((symbol, x) => {
                    let point = new Point(x, y);
                    switch (symbol) {
                        case TYPESYMBOLS.PLAYER:
                            tmp[x] = new PlayerModel(point);
                            break;
                        case TYPESYMBOLS.WALL:
                            tmp[x] = new WallModel(point);
                            break;
                        case TYPESYMBOLS.BOULDER:
                            tmp[x] = new BoulderModel(point);
                            break;
                        case TYPESYMBOLS.EMPTY:
                            tmp[x] = new EmptyModel(point);
                            break;
                        case TYPESYMBOLS.DIRT:
                            tmp[x] = new DirtModel(point);
                            break;
                        case TYPESYMBOLS.GEM:
                            tmp[x] = new GemModel(point);
                            break;
                        case TYPESYMBOLS.EXIT:
                            tmp[x] = new ExitModel(point);
                            break;
                        case TYPESYMBOLS.BOMB:
                            tmp[x] = new BombModel(point);
                            break;
                        default:
                            tmp[x] = new DirtModel(point);
                            break;
                    }
                });
                return tmp;
            }
        )
    );
};

const fireLoadEvent = () => {
    let evt = document.createEvent("HTMLEvents");
    evt.initEvent("levelLoaded",true,true);
    evt.eventName = "levelLoaded";
    document.getElementById("appContainer").dispatchEvent(evt);
};

export default loadLevel;
