import {ASSETS} from "../config";

export let canvas = null;
export let ctx = null;

const preload = () => {
    preloadAssets()
        .then(preloadCanvasContext)
        .then(fireLoadEvent)
        .catch((err) => {console.log(err)});
};

const preloadAssets = () => {
    return new Promise((resolve, reject) => {
        let ctr = 0;
        for (let asset in ASSETS) {
            ASSETS[asset].img.src = ASSETS[asset].src;
            ctr++;
            ASSETS[asset].img.addEventListener("load", () => {
                if (ctr === Object.keys(ASSETS).length) {
                    resolve();
                }
            });
            ASSETS[asset].img.addEventListener("error",reject);
        }
    });
};

const preloadCanvasContext = () => {
    canvas = document.getElementById("app");
    ctx = canvas.getContext("2d");
};

const fireLoadEvent = () => {
    let evt = document.createEvent("HTMLEvents");
    evt.initEvent("assetsLoaded",true,true);
    evt.eventName = "assetsLoaded";
    document.getElementById("appContainer").dispatchEvent(evt);
};

export default preload;
