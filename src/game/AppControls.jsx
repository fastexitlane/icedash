import {pauseLoops, startLoops} from "./GameLoop";
import loadLevel, {loadLevelFromFile, loadLevelFromShadowCopy} from "../loading/LevelLoader";
import {renderGrumpyFlappy} from "./Renderer";
import {AUDIOMUTEDONLOAD, LEVEL, STARTLEVEL, TIMEOUT_GAMENOTIFICATIONS, TIMEOUT_RENDER} from "../config";
import {
    boulderHitSound,
    btnLastLvl, btnNextLvl, btnPause, btnRestartLevel, btnResume, btnStart, explosionSound, gameMusic, gameWinSound,
    gemCollectedSound,
    infoPaused,
    lvlUploadWrapper,
    menuMusic
} from "../index";

let currentLevelKey = STARTLEVEL;
let externalFileMode = false;
let splashScreenTimeout = null;
let audioMuted = !AUDIOMUTEDONLOAD;

// level controls
export const startLevel = () => {
    clearTimeout(splashScreenTimeout);
    startLoops();
    enterGameMode();
};

export const nextLevel = () => {
    pauseLoops();
    if(confirm("Nächstes Level laden?\nAktueller Spielstand geht verloren.")) {
        if (currentLevelKey === Object.keys(LEVEL).length) {
            // return to first level
            currentLevelKey = 1;
        }
        else {
            currentLevelKey += 1;
        }
        loadLevel(currentLevelKey);
    }
    else {
        startLoops();
    }
};

export const lastLevel = () => {
    pauseLoops();
    if(confirm("Voriges Level laden?\nAktueller Spielstand geht verloren.")) {
        if (currentLevelKey === 1) {
            // return to highest level
            currentLevelKey = Object.keys(LEVEL).length;
        }
        else {
            currentLevelKey -= 1;
        }
        loadLevel(currentLevelKey);
    }
    else {
        startLoops();
    }
};

export const restartLevel = () => {
    pauseLoops();
    if(confirm("Neues Spiel starten?\nAktueller Spielstand geht verloren.")) {
        loadLevelFromShadowCopy(externalFileMode, currentLevelKey);
    }
    else {
        startLoops();
    }
};


// buttons
export const start = () => {
    loadLevel(currentLevelKey);
    btnStart.style.display = "none";
    lvlUploadWrapper.style.display = "none";
};

export const startFromLvlUpload = (evt) => {
    lvlUploadWrapper.style.display = "none";
    btnStart.style.display = "none";
    loadLevelFromFile(evt.target.files[0]);
    externalFileMode = true;
};

export const pause = () => {
    pauseLoops();
    splashScreen();
    enterPauseMode();
};

export const resume = () => {
    clearTimeout(splashScreenTimeout);
    enterGameMode();
    startLoops();
};

export const mute = () => {
    audioMuted = !audioMuted;

    menuMusic.muted = audioMuted;
    gameMusic.muted = audioMuted;
    gameWinSound.muted = audioMuted;
    explosionSound.muted = audioMuted;
    gemCollectedSound.muted = audioMuted;
    boulderHitSound.muted = audioMuted;
};


// event handlers
export const assetsLoaded = () => {
    menuMusic.play();
    splashScreen();
    btnStart.style.display = "inline-block";
};

export const playerDied = () => {
    btnRestartLevel.style.display = "none";
    explosionSound.play();
    explosionSound.currentTime = 0;
    setTimeout(() => {
        pauseLoops();
        alert("Kaputt! :-(");
        loadLevelFromShadowCopy(externalFileMode, currentLevelKey);
    }, TIMEOUT_GAMENOTIFICATIONS);
};

export const lvlWin = () => {
    setTimeout(() => {
        pauseLoops();
        alert("Gewonnen! :-)");
        if (externalFileMode) {
            location.reload();
        }
        else {
            nextLevel();
        }
    }, TIMEOUT_GAMENOTIFICATIONS/10);
};

export const timeOver = () => {
    explosionSound.play();
    explosionSound.currentTime = 0;
    setTimeout(() => {
        pauseLoops();
        alert("Zeit abgelaufen! :-(");
        loadLevelFromShadowCopy(externalFileMode, currentLevelKey);
    }, TIMEOUT_GAMENOTIFICATIONS/5)
};

export const playerTrapped = () => {
    setTimeout(() => {
        pauseLoops();
        alert("Gefangen! :-(");
        loadLevelFromShadowCopy(externalFileMode, currentLevelKey);
    }, TIMEOUT_GAMENOTIFICATIONS);
};


// helpers
const splashScreen = () => {
    splashScreenTimeout = setTimeout(splashScreen, TIMEOUT_RENDER);
    renderGrumpyFlappy();
};

const enterGameMode = () => {
    // hide pause controls
    infoPaused.style.display = "none";
    btnResume.style.display = "none";

    // stop menu music
    menuMusic.pause();
    menuMusic.currentTime = 0;

    // stop level win music
    gameWinSound.pause();
    gameWinSound.currentTime = 0;

    // display game controls
    if (!externalFileMode) {
        btnLastLvl.style.display = "inline-block";
        btnNextLvl.style.display = "inline-block";
    }

    btnRestartLevel.style.display = "inline-block";
    btnPause.style.display = "inline-block";

    // play game music
    gameMusic.play();
};

const enterPauseMode = () => {
    // hide game controls
    btnRestartLevel.style.display = "none";
    btnPause.style.display = "none";
    btnLastLvl.style.display = "none";
    btnNextLvl.style.display = "none";

    // stop game music
    gameMusic.pause();
    gameMusic.currentTime = 0;

    // show pause controls
    if (!externalFileMode) {
        btnLastLvl.style.display = "inline-block";
        btnNextLvl.style.display = "inline-block";
    }
    btnResume.style.display = "inline-block";
    infoPaused.style.display = "inline-block";

    // play menu music
    menuMusic.play();
};