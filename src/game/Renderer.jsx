import {canvas, ctx} from "../loading/Preloader";
import {ASSETS, TYPES} from "../config";
import DIRECTIONS from "../model/field/Directions";
import {currentLevel} from "../loading/LevelLoader";

const render = () => {
    const boxHeight = canvas.height / currentLevel.getFieldSize().height;
    const boxWidth = canvas.width / currentLevel.getFieldSize().width;

    ctx.clearRect(0, 0, canvas.width, canvas.height);

    currentLevel.field.forEach((row, y) => {
        row.forEach((cell, x) => {
            let asset = ASSETS[cell.code];

            // draw background
            ctx.drawImage(ASSETS.EMPTY.img, boxWidth*x, boxHeight*y, boxWidth, boxWidth);

            // draw invisible cells
            if (!cell.visible) {
                ctx.drawImage(ASSETS.INVISIBLE.img, boxWidth*x, boxHeight*y, boxWidth, boxWidth);
            }

            // draw visible cells
            else if (cell.visible) {
                // check player direction
                if (cell.code === TYPES.PLAYER && cell.currentDirection === DIRECTIONS.LEFT) {
                    asset = ASSETS.PLAYER_LEFT;
                }

                // check explosion
                if (cell.exploding) {
                    asset = ASSETS.EXPLOSION;
                }

                // static images w/o spritesheet
                if (!asset.sprite) {
                    ctx.drawImage(asset.img, boxWidth*x, boxHeight*y, boxWidth, boxWidth);
                }

                // dynamic images from spritesheet
                else if (asset.sprite) {
                    const spriteFrame = Math.floor(asset.frame % asset.sprites);
                    const spriteWidth = asset.img.width/asset.sprites;
                    const spriteHeight = asset.img.height;
                    ctx.drawImage(asset.img,
                        spriteFrame * spriteWidth,
                        0,
                        spriteWidth,
                        spriteHeight,
                        boxWidth*x,
                        boxHeight*y,
                        boxWidth,
                        boxHeight);
                }
            }
        })
    });

    Object.keys(ASSETS).forEach((assetKey) => {
        if (ASSETS[assetKey].sprite) {
            ASSETS[assetKey].frame += ASSETS[assetKey].frame_increase;
        }
    });

    document.getElementById("collectedGems").innerHTML = currentLevel.collectedGems.toString();
    document.getElementById("totalGems").innerHTML = currentLevel.totalGems.toString();
    document.getElementById("remainingTime").innerHTML = Math.floor(currentLevel.remainingTime/1000).toString();
};

export const renderGrumpyFlappy = () => {
    let asset = ASSETS[TYPES.PLAYER];

    ctx.clearRect(0, 0, canvas.width, canvas.height);

    const spriteFrame = Math.floor(asset.frame % asset.sprites);
    const frameWidth = asset.img.width/asset.sprites;
    const frameHeight = asset.img.height;
    ctx.drawImage(asset.img,
        spriteFrame * frameWidth,
        0,
        frameWidth,
        frameHeight,
        (canvas.width/2 - frameWidth/2),
        (canvas.height/2 - frameHeight/2),
        frameWidth,
        frameHeight
    );
    ASSETS[TYPES.PLAYER].frame += ASSETS[TYPES.PLAYER].frame_increase;
};

export const renderLevelKeyTitle = (currentLevel) => {
    document.getElementById("lvlKeyTitle").innerHTML = currentLevel.key.toString() + " - " + currentLevel.title.toString();
};

export default render;
