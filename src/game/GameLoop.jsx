import {TIMEOUT_GAME, TIMEOUT_RENDER} from "../config";
import {currentLevel} from "../loading/LevelLoader";
import render from "./Renderer";
import DIRECTIONS from "../model/field/Directions";

let direction = null;
let timeoutGame = null;
let timeoutRender = null;
let firstKeyPressed = false;
let gameRunning = false;

// add an event listener for keydown events and set the module-wide key code from the event
window.addEventListener("keydown", (evt) => {
    direction = handleInput(evt.keyCode);
    evt.preventDefault();

    if (gameRunning && !firstKeyPressed ) {
        firstKeyPressed = true;
        currentLevel.startTimer();
    }
});

export const startLoops = () => {
    gameRunning = true;
    gameTick();
    renderTick();
};

export const pauseLoops = () => {
    gameRunning = false;
    firstKeyPressed = false;
    clearTimeout(timeoutGame);
    clearTimeout(timeoutRender);
    currentLevel.stopTimer();
};

const gameTick = () => {
    timeoutGame = setTimeout(gameTick, TIMEOUT_GAME);
    //heckStatus();
    currentLevel.update(direction);
    direction = null;
};

const renderTick = () => {
    timeoutRender = setTimeout(renderTick, TIMEOUT_RENDER);
    render(currentLevel);
};

const handleInput = (keycode) => {
    switch (keycode) {
        // avoid rendering empty key codes
        default:
            return null;
        // arrow up
        case 38:
            return DIRECTIONS.UP;
        //arrow down
        case 40:
            return DIRECTIONS.DOWN;
        // arrow left
        case 37:
            return DIRECTIONS.LEFT;
        // arrow right
        case 39:
            return DIRECTIONS.RIGHT;
    }
};
