import GenericCelltype from "./GenericCelltype";
import DIRECTIONS from "../field/Directions";
import {TIMEOUT_CELLCONSUME, TYPES} from "../../config";

class PlayerModel extends GenericCelltype {
    constructor(position) {
        super(position);
        this.code = TYPES.PLAYER;
        this.consumable = true;
    }

    update(level, direction) {
        if (this.trapped(level)) {
            level.playerAlive = false;
            this.fireEvent("playerTrapped");
        }
        else if (Math.floor(level.remainingTime/1000) <= 0) {
            if (level.playerAlive) {
                level.playerAlive = false;
                level.stopTimer();
                this.exploding = true;
                this.fireEvent("timeOver");
            }
        }
        else if (direction !== null) {
            if (direction === DIRECTIONS.LEFT || direction === DIRECTIONS.RIGHT) {
                this.currentDirection = direction;
            }
            try {
                switch (level.getCell(this.position, direction).code) {
                    case TYPES.BOULDER:
                        let targetPos = level.getCell(this.position, direction).position;
                        if ((direction === DIRECTIONS.RIGHT || direction === DIRECTIONS.LEFT)
                            && (level.getCell(targetPos, direction).code === TYPES.EMPTY)) {
                            level.moveCell(targetPos, direction);
                            level.moveCell(this.position, direction);
                            break;
                        }
                    case TYPES.WALL:
                        break;
                    case TYPES.EXIT:
                        if (level.allGemsCollected()) {
                            level.moveCell(this.position, direction);
                            level.playerExited = true;
                            this.fireEvent("lvlWin");
                        }
                        break;
                    case TYPES.BOMB:
                        level.getCell(this.position, direction).explode(level);
                        break;
                    case TYPES.GEM:
                        level.collectedGems += 1;
                        this.fireEvent("gemCollected");
                    case TYPES.DIRT:
                    case TYPES.EMPTY:
                        level.moveCell(this.position, direction);
                }
                this.lastUpdated = new Date().getTime();
            }
            catch (err) {
                //console.log(err);
            }
        }
    }

    trapped(level) {
        try {
            return (
                (level.getCell(this.position, DIRECTIONS.DOWN).code === (TYPES.BOULDER || TYPES.WALL))
                && (level.getCell(this.position, DIRECTIONS.UP).code === (TYPES.BOULDER || TYPES.WALL))
                && (level.getCell(this.position, DIRECTIONS.LEFT).code === (TYPES.BOULDER || TYPES.WALL))
                && (level.getCell(this.position, DIRECTIONS.RIGHT).code === (TYPES.BOULDER || TYPES.WALL))
            )
        }
        catch (err) {
            //console.log(err);
        }
    }

    consume(level) {
        this.exploding = true;

        // fire playerDied event
        if (level.playerAlive) {
            level.playerAlive = false;
            this.fireEvent("playerDied");
        }


        setTimeout(() => {
            level.clearCell(this.position);
            }, TIMEOUT_CELLCONSUME
        );
    }
}

export default PlayerModel;