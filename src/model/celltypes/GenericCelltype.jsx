import {TIMEOUT_CELLCONSUME} from "../../config";

class GenericCelltype {
    constructor(position) {
        this.code = null;
        if (!position) {
            throw "ERROR: cell object without position reference instantiated!"
        } else {

            this.position = position;
        }
        this.lastUpdated = new Date().getTime();
        this.rounded = false;
        this.visible = true;
        this.currentDirection = null;
        this.consumable = false;
        this.exploding = false;
    }

    update() {
        this.lastUpdated = new Date().getTime();
    }

    consume(level) {
        this.exploding = true;
        setTimeout(() => {level.clearCell(this.position)}, TIMEOUT_CELLCONSUME);
    }

    fireEvent(evtName) {
        let evt = document.createEvent("HTMLEvents");
        evt.initEvent(evtName,true,true);
        evt.eventName = evtName;
        document.getElementById("appContainer").dispatchEvent(evt);
    }
}

export default GenericCelltype;