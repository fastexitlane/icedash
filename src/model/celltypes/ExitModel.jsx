import GenericCelltype from "./GenericCelltype";
import {TYPES} from "../../config";

class ExitModel extends GenericCelltype {
    constructor(position) {
        super(position);
        this.code = TYPES.EXIT;
        this.visible = false;
    }

    update(level) {
        if (level.allGemsCollected()) {
            this.visible = true;
        }
    }
}

export default ExitModel;