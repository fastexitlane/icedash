import GenericCelltype from "./GenericCelltype";
import {TYPES} from "../../config";
import DIRECTIONS from "../field/Directions";

class GemModel extends GenericCelltype {
    constructor(position) {
        super(position);
        this.code = TYPES.GEM;
        this.rounded = true;
        this.falling = false;
    }

    update(level) {
        try {
            // handle non-falling gem
            if (!this.falling) {
                // gem not falling, but can fall down
                if (level.getCell(this.position, DIRECTIONS.DOWN).code === TYPES.EMPTY) {
                    this.falling = true;
                }
                // gem can roll left
                else if ((level.getCell(this.position, DIRECTIONS.DOWN).rounded)
                    && (level.getCell(this.position, DIRECTIONS.LEFT).code === TYPES.EMPTY)
                    && (level.getCell(this.position, DIRECTIONS.DOWNLEFT).code === TYPES.EMPTY)) {
                    level.moveCell(this.position, DIRECTIONS.LEFT);
                }
                // gem can roll right
                else if ((level.getCell(this.position, DIRECTIONS.DOWN).rounded)
                    && (level.getCell(this.position, DIRECTIONS.RIGHT).code === TYPES.EMPTY)
                    && (level.getCell(this.position, DIRECTIONS.DOWNRIGHT).code === TYPES.EMPTY)) {
                    level.moveCell(this.position, DIRECTIONS.RIGHT);
                }
            }

            // handle falling gem
            else if (this.falling) {
                // gem falls straight down
                if ((level.getCell(this.position, DIRECTIONS.DOWN).code === TYPES.EMPTY)) {
                    level.moveCell(this.position, DIRECTIONS.DOWN);

                    if (level.getCell(this.position, DIRECTIONS.DOWN).code === TYPES.PLAYER) {
                        level.clearCell(this.position);
                        level.collectedGems += 1;
                    }
                }
                // gem falls to left
                else if ((level.getCell(this.position, DIRECTIONS.DOWN).rounded)
                    && (level.getCell(this.position, DIRECTIONS.LEFT).code === TYPES.EMPTY)
                    && (level.getCell(this.position, DIRECTIONS.DOWNLEFT).code === TYPES.EMPTY)) {
                    level.moveCell(this.position, DIRECTIONS.LEFT);
                }
                // gem falls to right
                else if ((level.getCell(this.position, DIRECTIONS.DOWN).rounded)
                    && (level.getCell(this.position, DIRECTIONS.RIGHT).code === TYPES.EMPTY)
                    && (level.getCell(this.position, DIRECTIONS.DOWNRIGHT).code === TYPES.EMPTY)) {
                    level.moveCell(this.position, DIRECTIONS.RIGHT);
                }
                // in all other cases, the gem isn't falling
                else {
                    this.falling = false;
                    this.fireEvent("gemHit");
                }
            }
            this.lastUpdated = new Date().getTime();
        }
        catch (err) {
            //console.log(err);
        }
    }
}

export default GemModel;