import GenericCelltype from "./GenericCelltype";
import {TYPES} from "../../config";

class WallModel extends GenericCelltype {
    constructor(position) {
        super(position);
        this.code = TYPES.WALL;
        this.consumable = true;
    }
}

export default WallModel;