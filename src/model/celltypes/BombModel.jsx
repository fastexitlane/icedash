import GenericCelltype from "./GenericCelltype";
import DIRECTIONS from "../field/Directions";
import {TYPES} from "../../config";
import Point from "../field/Point";

class BombModel extends GenericCelltype {
    constructor(position) {
        super(position);
        this.code = TYPES.BOMB;
        this.falling = false;
    }
    
    update(level) {
        /**
         * dropping bomb algorithm:
         *
         * 1 bomb is not falling
         * 1.1 bomb could be falling if lower cell is empty -> set it falling
         * 1.2 bomb can roll over to left -> move it left
         * 1.3 bomb can roll over to right -> move it right
         * 2 bomb is falling
         * 2.1 bomb can fall straight down -> move and check collision with new cell below
         * 2.1.1 collision with lower border of the field -> explode
         * 2.1.2 collision with wall or boulder -> explode
         * 2.1.3 collision with player -> consume player and bomb ("small explosion")
         * 2.2 bomb can roll over to left -> move it left
         * 2.3 bomb can roll over to right -> move it right
         * 3 catch out of bounds exceptions (do nothing)
         */

        try {
            // handle non-falling bomb
            if (!this.falling) {
                // bomb not falling, but can fall down
                if (level.getCell(this.position, DIRECTIONS.DOWN).code === TYPES.EMPTY) {
                    this.falling = true;
                }
                // bomb can roll left
                else if ((level.getCell(this.position, DIRECTIONS.DOWN).rounded)
                    && (level.getCell(this.position, DIRECTIONS.LEFT).code === TYPES.EMPTY)
                    && (level.getCell(this.position, DIRECTIONS.DOWNLEFT).code === TYPES.EMPTY)) {
                    level.moveCell(this.position, DIRECTIONS.LEFT);
                }
                // bomb can roll right
                else if ((level.getCell(this.position, DIRECTIONS.DOWN).rounded)
                    && (level.getCell(this.position, DIRECTIONS.RIGHT).code === TYPES.EMPTY)
                    && (level.getCell(this.position, DIRECTIONS.DOWNRIGHT).code === TYPES.EMPTY)) {
                    level.moveCell(this.position, DIRECTIONS.RIGHT);
                }
            }

            // handle falling bomb
            else if (this.falling) {
                // bomb falls straight down
                if ((level.getCell(this.position, DIRECTIONS.DOWN).code === TYPES.EMPTY)) {
                    level.moveCell(this.position, DIRECTIONS.DOWN);

                    // check explodability:
                    // 1) explode on collision with lower border
                    if (this.position.y === (level.getFieldSize().height - 1)) {
                        this.explode(level);
                    }

                    // 2) handle collision with other fields
                    else {
                        switch (level.getCell(this.position, DIRECTIONS.DOWN).code) {
                            // explode on collision with a wall or boulder cell
                            case TYPES.WALL:
                            case TYPES.BOULDER:
                                this.explode(level);
                                break;
                            // consume player and bomb on player collision
                            case TYPES.PLAYER:
                                level.getCell(this.position, DIRECTIONS.DOWN).consume(level);
                                this.consume(level);
                                break;
                        }
                    }
                }

                // bomb falls to left
                else if ((level.getCell(this.position, DIRECTIONS.DOWN).rounded)
                    && (level.getCell(this.position, DIRECTIONS.LEFT).code === TYPES.EMPTY)
                    && (level.getCell(this.position, DIRECTIONS.DOWNLEFT).code === TYPES.EMPTY)) {
                    level.moveCell(this.position, DIRECTIONS.LEFT);
                }

                // bomb falls to right
                else if ((level.getCell(this.position, DIRECTIONS.DOWN).rounded)
                    && (level.getCell(this.position, DIRECTIONS.RIGHT).code === TYPES.EMPTY)
                    && (level.getCell(this.position, DIRECTIONS.DOWNRIGHT).code === TYPES.EMPTY)) {
                    level.moveCell(this.position, DIRECTIONS.RIGHT);
                }

                // in all other cases, the bomb isn't falling any more
                else {
                    this.falling = false;
                }
            }
            this.lastUpdated = new Date().getTime();
        }
        catch (err) {
            //console.log(err);
        }
    }
    
    explode(level) {
        // clear the cells
        Object.keys(DIRECTIONS).forEach((direction) => {
            try {
                let target = new Point(this.position.x + DIRECTIONS[direction].x, this.position.y + DIRECTIONS[direction].y);
                if (level.getCell(target).consumable) {
                    level.getCell(target).consume(level);
                }
            }
            catch (err) {
                //console.log(err);
            }
        });

        // consume itself
        this.consume(level);

        // fire explosion event
        this.fireEvent("explosion");
    }
}

export default BombModel;