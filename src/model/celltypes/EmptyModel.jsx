import GenericCelltype from "./GenericCelltype";
import {TYPES} from "../../config";

class EmptyModel extends GenericCelltype {
    constructor(position) {
        super(position);
        this.code = TYPES.EMPTY;
    }
}

export default EmptyModel;