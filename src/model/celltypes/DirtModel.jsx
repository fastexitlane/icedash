import GenericCelltype from "./GenericCelltype";
import {TYPES} from "../../config";

class DirtModel extends GenericCelltype {
    constructor(position) {
        super(position);
        this.code = TYPES.DIRT;
        this.consumable = true;
    }
}

export default DirtModel;