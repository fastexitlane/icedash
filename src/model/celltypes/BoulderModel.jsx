import GenericCelltype from "./GenericCelltype";
import DIRECTIONS from "../field/Directions";
import {TYPES} from "../../config";

class BoulderModel extends GenericCelltype {
    constructor(position) {
        super(position);
        this.code = TYPES.BOULDER;
        this.rounded = true;
        this.falling = false;
        this.consumable = true;
    }

    update(level) {
        try {
            // handle non-falling boulder
            if (!this.falling) {
                // boulder not falling, but can fall down
                if (level.getCell(this.position, DIRECTIONS.DOWN).code === TYPES.EMPTY) {
                    this.falling = true;
                }
                // boulder can roll left
                else if ((level.getCell(this.position, DIRECTIONS.DOWN).rounded)
                    && (level.getCell(this.position, DIRECTIONS.LEFT).code === TYPES.EMPTY)
                    && (level.getCell(this.position, DIRECTIONS.DOWNLEFT).code === TYPES.EMPTY)) {
                    level.moveCell(this.position, DIRECTIONS.LEFT);
                }
                // boulder can roll right
                else if ((level.getCell(this.position, DIRECTIONS.DOWN).rounded)
                    && (level.getCell(this.position, DIRECTIONS.RIGHT).code === TYPES.EMPTY)
                    && (level.getCell(this.position, DIRECTIONS.DOWNRIGHT).code === TYPES.EMPTY)) {
                    level.moveCell(this.position, DIRECTIONS.RIGHT);
                }
            }

            // handle falling boulder
            else if (this.falling) {
                // boulder falls straight down
                if ((level.getCell(this.position, DIRECTIONS.DOWN).code === TYPES.EMPTY)) {
                    level.moveCell(this.position, DIRECTIONS.DOWN);

                    if (level.getCell(this.position, DIRECTIONS.DOWN).code === TYPES.PLAYER) {
                        level.getCell(this.position, DIRECTIONS.DOWN).consume(level);
                        //level.moveCell(this.position, DIRECTIONS.DOWN);
                    }

                    if (level.getCell(this.position, DIRECTIONS.DOWN).code === TYPES.BOMB) {
                        level.getCell(this.position, DIRECTIONS.DOWN).explode(level);
                    }
                }
                // boulder falls to left
                else if ((level.getCell(this.position, DIRECTIONS.DOWN).rounded)
                    && (level.getCell(this.position, DIRECTIONS.LEFT).code === TYPES.EMPTY)
                    && (level.getCell(this.position, DIRECTIONS.DOWNLEFT).code === TYPES.EMPTY)) {
                    level.moveCell(this.position, DIRECTIONS.LEFT);
                }
                // boulder falls to right
                else if ((level.getCell(this.position, DIRECTIONS.DOWN).rounded)
                    && (level.getCell(this.position, DIRECTIONS.RIGHT).code === TYPES.EMPTY)
                    && (level.getCell(this.position, DIRECTIONS.DOWNRIGHT).code === TYPES.EMPTY)) {
                    level.moveCell(this.position, DIRECTIONS.RIGHT);
                }
                // in all other cases, the boulder isn't falling
                else {
                    this.fireEvent("boulderHit");
                    this.falling = false;
                }
            }
            this.lastUpdated = new Date().getTime();
        }
        catch (err) {
            //console.log(err);
        }
    }
}

export default BoulderModel;