import EmptyModel from "./celltypes/EmptyModel";
import Point from "./field/Point";
import {LEVEL_TIME, TYPES} from "../config";

class LevelModel {
    constructor(field = [[]], key=0, title="external", time=LEVEL_TIME) {
        this.key = key;
        this.title = title;
        this.field = field;
        this.totalGems = this.getGemCount();
        this.collectedGems = 0;
        this.playerAlive = true;
        this.timerRunning = false;
        this.lastTimerUpdate = null;
        this.remainingTime = time;
    }

    update(direction) {
        const currentTimestamp = new Date().getTime();
        this.field.forEach((row) => {
            row.forEach((cell) => {
                if ((cell.lastUpdated < currentTimestamp) && !cell.exploding) {
                    cell.update(this, direction);
                }
            })
        });

        // update timer
        if (this.timerRunning) {
            this.remainingTime = this.remainingTime - (currentTimestamp - this.lastTimerUpdate);
            this.lastTimerUpdate = new Date().getTime();
        }
    }

    startTimer() {
        this.lastTimerUpdate = new Date().getTime();
        this.timerRunning = true;
    }

    stopTimer() {
        this.timerRunning = false;
    }

    getFieldSize() {
        return {height: this.field.length, width: this.field[0].length};
    }

    getGemCount() {
        let ctr = 0;
        this.field.forEach((row) => {
            row.forEach((cell) => {
                if (cell.code === TYPES.GEM) {
                    ctr++;
                }
            })
        });
        return ctr;
    }

    getCell(point, direction={x:0, y:0}) {
        if (((point.x + direction.x) <= (this.getFieldSize().width -1 ))
            && ((point.y + direction.y) <= (this.getFieldSize().height - 1))
            && (point.x + direction.x >= 0)
            && (point.y + direction.y >= 0)) {
            return this.field[point.y + direction.y][point.x + direction.x];
        }

        else {
            throw "INFO: Field out of Bounds";
        }
    }

    setCell(point, cellobj) {
        if ((point.x <= (this.getFieldSize().width - 1))
            && (point.y <= (this.getFieldSize().height - 1))) {
            this.field[point.y][point.x] = cellobj;
        }

        else {
            throw "INFO: Field out of Bounds";
        }
    }

    clearCell(point) {
        this.setCell(point, new EmptyModel(point));
    }

    moveCell(point, direction) {
        let target = new Point((point.x + direction.x), (point.y + direction.y));

        if ((target.x <= (this.getFieldSize().width - 1))
            && (target.y <= (this.getFieldSize().height - 1))) {
            this.setCell(target, this.getCell(point));
            this.getCell(target).position = target;
            this.clearCell(point);
        }
        else {
            throw "INFO: Field out of Bounds";
        }
    }

    allGemsCollected() {
        return (this.collectedGems === this.totalGems);
    }
}

export default LevelModel;
