const DIRECTIONS = {
    UP         :   {x: 0, y: -1},
    DOWN       :   {x: 0, y: 1},
    RIGHT      :   {x: 1, y: 0},
    LEFT       :   {x: -1, y: 0},
    UPGRIGHT   :   {x: 1, y: -1},
    UPLEFT     :   {x: -1, y: -1},
    DOWNRIGHT  :   {x: 1, y: 1},
    DOWNLEFT   :   {x: -1, y: 1},
};

export default DIRECTIONS;