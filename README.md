# IceDash
Dieser Rebuild des C64-Klassikers "Boulderdash" entstand im Rahmen der Vorlesung "Projekte der Wirtschaftsinformatik" (Q3/2017) an der FHDW in Bergisch Gladbach.

**(i)** Das Spiel kann [hier](https://fastexitlane.com/projects/boulderdash_app/) (Public) oder [hier](https://fastexitlane.gitlab.io/icedash) (Development) gespielt werden.

**(i)** Der Code kann auf [GitLab](https://gitlab.com/fastexitlane/icedash) eingesehen werden.


## Release-Informationen
**Release**: IceDash 1.0

### Browserkompatibilität
Die folgenden Browser werden vollständig unterstützt:
* Google Chrome (Microsoft Windows, Apple macOS, Linux)
* Mozilla Firefox (Microsoft Windows, Apple macOS, Linux)

Die folgenden Browser werden teilweise unterstützt:
* Apple Safari (Apple macOS)

Die folgenden Browser werden nicht unterstützt:
* Microsoft Internet Explorer
* Microsoft Edge

### Known Bugs
* Sind in einem Level zwei Spieler vorhanden (Level 2), kann der zweite Spieler noch einige Sekunden weiter bewegt werden, wenn der erste Spieler durch eine Bombe oder einen Stein stirbt (umfangreicheres Architektur-Problem aufgrund der Trennung von Datenmodell-Update und Rendering).
* Hintergrund-Sounds werden bei Meldungen (`alert`, `confirm` etc.) unterbrochen, da diese den JavaScript-Main-Thread anhalten (Lösung durch Implementierung eigener Meldungen möglich).
* Soundeffekte können in Apple Safari nicht abgespielt werden, da der Browser eine eigene Audio-API für JavaScript verwendet (WebKit Audio Context).


## Contributing
Dieses Projekt setzt auf vanilla HTML, CSS und JavaScript im ES6-Standard auf.
Die Einbindung von Libraries ist generell nicht vorgesehen.
Im Sinne der Abwärtskompatibilität zu ES5-JavaScript wird allerdings `webpack` im Zusammenspiel mit dem `babel`-Loader im ES2015-Sprachstandard eingesetzt.
Die lokale Entwicklung wird durch den `webpack-dev-server` unterstützt.

Um das Projekt lokal aufsetzen, sind die folgenden Schritte erforderlich:
1. [Node.js](https://nodejs.org/de/) (6er-Release-Zyklus, aktuellster LTS-Release) mit npm (Node Package Manager) installieren
1. Repository klonen: `git clone git@github.com:fastexitlane/icedash.git`
2. Abhängigkeiten des Projekts lokal installieren: `npm install`

Es sind folgende `npm`-Targets definiert:
* `npm run dev`: startet den `webpack-dev-server` auf Port 8080
* `npm run build`: kompiliert den ES6-Sourcecode zur `bundle.js`, sodass diese (gemeinsam mit dem restlichen statischen Content) über einen beliebigen Webserver bereitgestellt werden kann


## Architektur
### Verzeichnisse und Dateien
Zu den relevanten Verzeichnissen und Dateien zählen:
* `src/`: JSX-Sourcecode
* `build/`: generierter ES5-JavaScript-Quellcode, der im Browser genutzt wird (`bundle.js`)
* `css/`: CSS-Dateien
* `webpack.config.js`: Konfigurationsdatei für `webpack` (insb. Entry Point für die Kompilierung, Compiler-Output, Einbindung des `babel`-Compilers)
* `package.json`: Konfigurationsdatei für `npm` (insb. Abhängigkeiten des Projekts, Skripte bzw. `npm`-Targets, Projektdetails)
* `src/config.jsx`: allgemeine Konfigurationsparameter und Konstanten der Anwendung selbst

### Drei-Layer-Aufbau
Die Architektur des Spiels setzt sich im Wesentlichen aus drei Ebenen (Layern) zusammen:
1. **Model-Layer** (`src/model`): Der Model-Layer kapselt das Datenmodell des Spiels (Level). Daneben beschreibt er die innere Spiellogik, d.h. das Verhalten der einzelnen Feldtypen. Er basiert als einziger Layer vollständig auf dem Klassenkonzept von ES6.
2. **Loading-Layer** (`src/loading`): Im Loading-Layer werden das Preloading der Assets (nach intialem Seitenaufbau) sowie die Initialisierung eines Levels durchgeführt.
3. **Game-Layer** (`src/game`): Der Game Layer bildet die "äußere" Spiellogik, d.h. Benutzerführung und GameLoop, sowie das Rendering des Spiels ab.

Details zu den einzelnen Layern werden im Wiki beschrieben.


## Credits
IceDash wird durch folgende Drittanbieter-Assets ermöglicht:
* flappy-grumpy ([http://bevouliin.com](http://bevouliin.com), Open Game Art License / OGA-BY)
* Coin Animation ([dontmind8.blogspot.com](dontmind8.blogspot.com), Creative Commons / CC-BY)
* Platformer Art Deluxe ([kenney.nl](kenney.nl), Creative Commons / CC0)
* Explosion Tilesets ([elineo](https://opengameart.org/content/explosion-tilesets)), Creative Commons / CC0)
* Game-Game ([metaruka](https://opengameart.org/content/game-game), Creative Commons / CC-BY-SA)
* RPG Sound Pack ([artisticdude](https://opengameart.org/content/rpg-sound-pack), Creative Commons / CC0)

Wesentlich an dem Projkekt mitgewirkt haben:
* Ramona Engelen (Design, Testing)
* Marco Dombrowski (Level Design, Testing)
* Yanik Neuenhaus (Lizenzierung)
* Dominik Becker (Development)
